#!/usr/bin/bash -x

#LD_LIBRARY_PATH=/usr/lib/libeatmydata
LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu
#LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libeatmydata.so

PROJECT=5407120

export LD_LIBRARY_PATH LD_PRELOAD
export USENETWORK=yes

# -----------------------------------
# functions
end () {
 rm  /tmp/pb.pid
 exit $1
}
# -----------------------------------

date
pgrep gpg-agent || exit 1
echo | gpg --batch --sign > /dev/null

# check pid
if test -f /tmp/pb.pid; then
    figlet oops pid !
    exit 1;
fi
echo $$ > /tmp/pb.pid


sudo echo
time sudo eatmydata -- pbuilder clean
sudo mkdir /var/cache/pbuilder/aptcache

while getopts 'cbfnd:k:i:s:p:r:z:x:e:a:l:t:u:y:' c
do
  case $c in
    c) create=1;;
    b) build=1;;
    f) fetch=1;;
    n) nightly=1;;
    d) deb_codename=$OPTARG;;
    k) koha_ver=$OPTARG;;
    i) koha_ver_incr=$OPTARG;;
    s) ksuite=$OPTARG;;
    p) aptpath=$OPTARG;;
    r) repo=$OPTARG;;
    z) repopath=$OPTARG;;
    x) suffix=$OPTARG;;
    e) bin=$OPTARG;;
    a) add=$OPTARG;;
    l) localpath=$OPTARG;;
    t) tmpdir=$OPTARG;;
    u) urgency=$OPTARG;;
    y) kbranch=$OPTARG;;
  esac
done

case $deb_codename  in
  "jessie")     dv='deb8';;
  "stretch")    dv='deb9';;
  "buster")     dv='deb10';;
  "bullseye")   dv='deb11';;
  "bookworm")   dv='deb12';;
esac
deb_rel=$dv

#kbranch="$ksuite.x";

case $ksuite  in
  "exp")            kbranch='master';;
  "unstable")       kbranch='master';;
  "dev")            kbranch='master';;
  "testing")        kbranch='master';;
  "stable")         kbranch='23.11.x';;
  "oldstable")      kbranch='23.05.x';;
  "oldoldstable")   kbranch='22.11.x';;
esac
#if null then assign
[[ -z "$kbranch" ]] && kbranch="$ksuite.x"

figlet $kbranch

if ! [  "$localpath"  ]; then
#  localpath='~/pb/pb_master/koha'
    exit
fi

if ! [  "$urgency"  ]; then
  urgency='medium'
fi

echo $localpath
echo $kbranch

cd $localpath

rm -f ../koha_*

echo $ksuite
echo $build
echo $koha_ver_incr
echo $add
echo "add = $add"

if ! [[ "$build" -eq 1 ]]; then
  build=0
fi

sudo rm -rf ./qa-test-tools
echo $koha_ver

# update head
# --------------------------
# --------------------------
if [  "$fetch"  ]; then

  sudo rm -rf ./qa-test-tools
  time sudo git clean -ffdx
  time sudo git reset --hard -q
  time git checkout -f master

  time git reset --hard origin/master
  time git fetch -f --tag

  git branch --set-upstream-to=origin/master  master
  git pull -q
  git push -u gitlab master

  git branch -D pb_master
  git branch pb_master ;  git checkout pb_master
  git branch --set-upstream-to=origin/master  pb_master

  if [ "$ksuite" ==  'unstable' ] || [ "$ksuite" ==  'testing' ]; then
    git pull -f
  else
    git pull --tag --no-edit --force
  fi

  time git reset --hard origin/$kbranch  -q
fi

# --------------------------
# check for last successful commit? and quit
commit=` git rev-parse --short=8 HEAD `
commit_last=`cat $repopath/deb/$ksuite-$PROJECT.success`

echo $commit
echo $commit_test

if [ "$commit_last" ]; then
  if [ "$commit_last" == "$commit" ]; then
    echo 'already built this commit, exiting...'
    date
    #        git checkout pb_master
#    exit 1
    end 1
  fi
fi
echo 'no match, continue...';

# --------------------------
echo $koha_ver
if [ "$nightly" ]; then
  codename=$ksuite
  koha_ver=`grep '^$VER'  ./Koha.pm | cut -d '"' -f2`
  koha_shortver=`echo  "$koha_ver"   | rev | cut -c5- | rev `

  koha_ver_db=`echo  "$koha_ver"    | cut -c10-   | sed 's/^0*//' `
  echo $koha_ver_db
  if [ ! "$koha_ver_db" ]; then  # force 0 if null
    koha_ver_db='0'
  fi

  koha_ver="$koha_shortver-$koha_ver_db"
else
  codename=`echo "$koha_ver"  | perl -pe 's/\.\d\d$//'`
  koha_shortver=$koha_ver
fi

echo $koha_ver_db

date=`date -Ins | cut -f1 -d',' | tr -cd '[:digit:]'`
name="$koha_ver~git+"

echo $name
echo $namelong
echo $koha_ver
echo $koha_shortver

base="base_$koha_shortver+$deb_rel"
pbase=$base

if [ $suffix ] ; then
  pbase="$base-$suffix"
fi

echo $base

# --------------------------
# set branch and vars
if [   "$fetch"  ]; then

  namelong="$koha_ver~git+$date.$commit"
  koha_branch=`echo "$namelong"  | perl -pe 's/~/-/' `
  echo   $koha_branch

  git branch "pb_$koha_branch"
  git branch -D "pb_$ksuite" ; git branch "pb_$ksuite"
  time git checkout "pb_$koha_branch"

  if [ ! $nightly  ]; then
    koha_tag="v$koha_ver"
  fi
  time git reset --hard  $koha_tag

else
  namelong="$koha_ver~git+$date.$commit"
fi

# --------------------------
mkdir  ~/$tmpdir/$namelong

if [ ! $koha_ver_incr  ]; then
  koha_ver_incr=1
fi

sudo git log --oneline | head

export PBUILDERSATISFYDEPENDSCMD="/usr/lib/pbuilder/pbuilder-satisfydepends-gdebi"
#export

time sudo apt-get -y install eatmydata
sudo ln -s /usr/lib/x86_64-linux-gnu/libeatmydata.so /usr/lib/libeatmydata.so

time sudo apt-get -y install devscripts pbuilder dh-make fakeroot debian-archive-keyring ubuntu-archive-keyring debhelper  build-essential  devscripts debhelper dh-make-perl gdebi-core bash-completion figlet

# -----------------

koha_rel_name="$koha_ver-$koha_ver_incr"
if [ $nightly  ]; then
  koha_rel_name="$namelong-$koha_ver_incr"
fi

echo $koha_ver
echo $koha_ver_incr
echo $koha_rel_name
echo $koha_ver_db

# -----------------

if [ ! -f "/var/cache/pbuilder/$pbase.tgz" ]; then
  create=1
fi

if [[ $create ]]; then
  figlet create pbuild

  time sudo git clean -ffdx

  time sudo LD_PRELOAD=/usr/lib/libeatmydata.so eatmydata -- pbuilder create --mirror http://apt.kohaaloha.com:3142/ftp.nz.debian.org/debian \
  --distribution $deb_codename  --basetgz /var/cache/pbuilder/$pbase.tgz --use-network yes
fi

time sudo eatmydata -- pbuilder execute --save-after-login \
     --bindmounts $localpath  --basetgz /var/cache/pbuilder/$pbase.tgz \
      $bin/prep-pbuilder.sh -s $ksuite -r "$aptpath" -c $codename -b $build \
    -p $localpath -z $repo --use-network yes

if [ $urgency == 'high'  ]; then
    change_title="New upstream SECURITY release ($koha_ver)"
else
    change_title="New upstream release ($koha_ver)"
fi

DEBEMAIL=mtj@kohaaloha.com  DEBFULLNAME='Mason James'  dch \
--force-bad-version \
--urgency $urgency \
--distribution $codename  --force-distribution -v "$koha_rel_name" "$change_title"

# ---------------------------------------
git commit -m "Update debian/control file: $koha_ver on $deb_rel ($commit)" debian/control

#rc=$?
# if control commit succeeds, copy patch to deb dir
if [ $? -eq 0 ]; then

  pwd  ~/$tmpdir
  pwd  ~/$tmpdir/$namelong

  echo "git format-patch -n --stdout HEAD~1 > ~/$tmpdir/$namelong/control.patch"
  git format-patch -n --stdout HEAD~1 > ~/$tmpdir/$namelong/control.patch

fi
# ---------------------------------------

git commit -m "Update debian/changelog file: $koha_ver" debian/changelog

# if control commit succeeds, copy patch to deb dir
if [ $? -eq 0 ]; then
  pwd  ~/$tmpdir
  pwd  ~/$tmpdir/$namelong
  echo "git format-patch -n --stdout HEAD~1 > ~/$tmpdir/$namelong/changelog.patch"
  git format-patch -n --stdout HEAD~1 > ~/$tmpdir/$namelong/changelog.patch
fi

git branch -D "pb_$ksuite.latest" ; git branch "pb_$ksuite.latest"
pkgname=$namelong

if [[ ! $nightly ]]; then
  pkgname=$koha_ver
fi

sudo chmod -R 777 ./node_modules

figlet add skip
echo 'tar-ignore = "node_modules"' > debian/source/options

cat <<'EOF' > debian/clean
node_modules/
EOF

git rm .gitignore
git commit -m 'rm .gitignore' .gitignore

git add   debian/source/options
git add   debian/clean
git commit -m "skip node_modules" debian/source/options  debian/clean

# ---------------------------------------

echo $pkgname
yarn -V
yarn config list
yarn config get httpProxy
#yarn list

time sudo  WEBSERVER_IP=192.168.1.65 \
        DEB_BUILD_OPTIONS=nocheck \
            $bin/build-git-snapshot \
                -d \
                -v $pkgname \
                -D $codename \
                -b $pbase \
                --noautoversion \
                -r  ~/$tmpdir/$namelong \
                -i $koha_ver_incr

#pdebuild  --debbuildopts -sa  -- --basetgz /var/cache/pbuilder/base_19.11.06+d8.tgz --buildresult /home/mason/$tmpdir/19.11.06~git+d8.20200527052905.3118478d
#time sudo WEBSERVER_IP=192.168.1.47  DEB_BUILD_OPTIONS=nocheck  pdebuild --debbuildopts -sa --buildresult /home/mason/debian --logfile /tmp/aa

# if build-git-snapshot succeeds...
bgs_rc=$?

#yarn list

if [ $bgs_rc -eq 0 ]; then

  # sign packages
  debsign --no-re-sign -k3EA44636DBCE457DA2CF8D823C9356BBA2E41F10 ~/$tmpdir/$namelong/*.changes
  git push -u gitlab "pb_$koha_branch"

  gpg --verify  ~/$tmpdir/$namelong/*.dsc
  rc=$?
  if ! [ $rc -eq 0 ]; then
    echo 'debsign failed, exiting...'
#    exit 1
    end 1
  fi

  # copy files to deb dir
#  rm ~/pb/$tmpdir
  ln -s -f  ~/$tmpdir/$namelong ~/pb/$tmpdir
  mkdir $repopath/deb
  cd   $repopath/deb

  rm ./$codename ./$ksuite ./$kbranch
  cp -av  ~/$tmpdir/$namelong .
  ln -s -f  ./$namelong ./$codename
  ln -s -f  ./$namelong ./$ksuite
  ln -s -f  ./$namelong ./$kbranch
  ##  cp /var/cache/pbuilder/$pbase.tgz ./$ksuite/pbuilder-$pbase.tgz

  cd   $repopath/deb/$ksuite
  ##  ln -s -f ./pbuilder-$pbase.tgz ./pbuilder-base.tgz
  ln -s -f ./*.dsc ./dsc
  ln -s -f ./*.changes  ./changes
  ln -s -f ./*.buildinfo  ./buildinfo
  ln -s -f ./*.orig.tar.gz ./koha.orig.tar.gz
  ln -s -f ./koha-common_*all.deb   ./koha-common_all.deb

  cp  $localpath/debian/control* .

  # -----------
  if [[ $add == 'local' || $add == 'remote'  ]]; then
    deps="koha koha-common koha-deps koha-perldeps koha-core koha-full"

    for dep in $deps; do
      reprepro --ignore=uploaders -b $repopath remove $codename $dep
    done

    reprepro --ignore=uploaders -b $repopath clearvanished
    reprepro --ignore=uploaders -b $repopath deleteunreferenced


    if [[ $repo == 'koha' ]]; then
      reprepro --ignore=uploaders -b $repopath include  $codename   ~/$tmpdir/$namelong/*.changes
    else
      reprepro --ignore=uploaders -b $repopath includedeb  $codename   ~/$tmpdir/$namelong/*.deb
    fi

    if [[ $add == 'remote' ]]; then
      # rsync to kc-deb
      time rsync -av  --delete --exclude=.git --exclude=deb \
      $repopath  \
      robin@debian.koha-community.org:koha-apt-repository
    fi
  fi

  # -----------
  rm  ~/$tmpdir/last
  ln -s -f ~/$tmpdir/$namelong   ~/$tmpdir/last
  #  sudo rm -rf ~/$tmpdir/$namelong
  # -----------

  # add koha-l10n, if not nightly
    if [[ ! "$nightly" ]]; then
        figlet 'add koha-l10n'
        cd /home/mason/git/l10n
        #  perl   ./build-l10n.pl -t v23.05.06  -b 23.05
        echo "v$koha_ver"
        echo $kbranch
        perl ./build-l10n.pl -t v$koha_ver -b $kbranch
    fi

  echo $commit > $repopath/deb/$ksuite-$PROJECT.success

#perl /home/mason/git/pkg-bot/bot-client.pl  -e -s "koha $kbranch packages pushed to koha-staging repo"

#perl /home/mason/git/pkg-bot/bot-client.pl  -e -b $kbranch -r $repo -c $commit

  figlet ok
else
  echo "bgs error $bgs_rc"
fi

echo "koha_shortver = $koha_shortver"
date
end
