#!/bin/bash -x

while getopts 'b:s:r:p:c:z:s' c
do
  case $c in
    b) build=$OPTARG;;
    s) ksuite=$OPTARG;;
    r) repopath=$OPTARG;;
    z) repo=$OPTARG;;
    p) path=$OPTARG;;
    c) codename=$OPTARG;;
  esac
done

echo '------------------------';
echo $build
echo $ksuite
echo $repopath
echo $repo
echo $codename
echo '------------------------';

#EXTRAPACKAGES=eatmydata
echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup
#ln -s  /usr/lib/x86_64-linux-gnu/libeatmydata.so   /usr/lib/libeatmydata.so

apt-get -y install \
    ca-certificates \
    wget \
    curl \
    gnupg2 \
    sudo \
    libmodern-perl-perl \
    apt-file

wget -q -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -

if ! [[ "$build" -eq 1 ]]; then
    build=0
    echo $build
fi

if [ $ksuite ==  'unstable' ] || [ $ksuite == 'dev' ]; then
    codename='dev'
fi

if [ $ksuite ==  'testing' ]; then
    codename='testing'
fi

echo "$repopath" >  /etc/apt/sources.list.d/koha.list
cat   /etc/apt/sources.list.d/koha.list

apt-get -y update
apt-get -y upgrade

apt install -y \
    tree \
    figlet \
    apt-file \
    libsys-cpu-perl \
    libcpan-meta-perl \
    libmodern-perl-perl \
    libmodule-cpanfile-perl \
    libparallel-forkmanager-perl \
    libcache-memcached-fast-safe-perl

## Add Node.js
#wget -O- -q https://deb.nodesource.com/setup_18.x | bash -
#
# Pin Node.js v18
cat <<'EOF'  > /etc/apt/preferences.d/nodejs
Package: nodejs
Pin: version 18.*
Pin-Priority: 999
EOF

sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

NODE_MAJOR=18
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list

sudo apt-get update

# Add yarn repo
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
   && wget -O- -q https://dl.yarnpkg.com/debian/pubkey.gpg \
      | gpg --dearmor \
      | tee /usr/share/keyrings/yarnkey.gpg >/dev/null \
   && echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list

# Install Node.js (includes npm) and Yarn

sudo apt-get update
sudo apt-get -y install nodejs yarn

mkdir -p /nonexistent/.yarn
mkdir -p /nonexistent/.cache/yarn

chmod -R 777 /nonexistent

# Install gulp
figlet Install gulp
npm install gulp-cli -g

yarn --version
yarn config set httpProxy http://192.109.1.40:3128
yarn config get httpProxy

node -v
which gulp
gulp
gulp -v

apt-get -y install pigz
rm /bin/gzip
rm /bin/gunzip
ln -s /usr/bin/pigz /bin/gzip
ln -s /usr/bin/unpigz /bin/gunzip

apt-file update

cd $path

echo '------------------------';
if [[ $build -eq 1 ]]; then
   #   time PERL5LIB=/home/mason/pb/pb_master debian/update-control

   # diff control.in file
   curl http://apt.kohaaloha.com/$repo/deb/$codename/control.in -o control.in2 -q

   md5sum ./debian/control.in
   md5sum ./control.in2

   diff  ./debian/control.in ./control.in2

#   if [ $? -eq 1 ]; then
   if [ 1 ]; then
        figlet 'force control'
        time PERL5LIB=$path  debian/update-control
        rm ./control.in2
   else
        figlet 'sync control'
        curl http://apt.kohaaloha.com/$repo/deb/$codename/control -o ./debian/control -q
#        diff ./debian/control

   fi
   rm ./control.in2
   cd /home/mason/pb/pb-nightly/koha
fi

rm -rf /var/cache/apt/archives/*

figlet nonexistent
sudo chmod -R 777 /nonexistent/.cache
sudo tree -f /nonexistent
