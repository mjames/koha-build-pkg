#!/bin/bash -x

date

#LD_LIBRARY_PATH=/usr/lib/libeatmydata
#LD_PRELOAD=libeatmydata.so
#export LD_LIBRARY_PATH LD_PRELOAD

localpath="/home/mason/pb/pb-nightly/koha"

#kohaver="20.05.01"
#kohaver="master"
#kohaver_incr="1"

#debver="stretch"
debver="stretch"
#debver="buster"
#debver="bionic"
#suite="unstable"

#suite="unstable"
suite="dev"

repo="koha-staging"

repopath="/home/mason/nfs/apt/kc/$repo"

#aptrepo="deb [trusted=yes] http://debian.koha-community.org/koha-staging $suite main"
#aptrepo="deb http://debian.koha-community.org/koha-staging $suite main"
aptrepo="deb http://apt.kohaaloha.com:3142/debian.koha-community.org/koha-staging $suite main"

suffix="kc"
tmpdir="tmp-$suffix"

if [ ! -d ~/$tmpdir ] ; then
    mkdir ~/$tmpdir
fi

bin="/home/mason/git/bin-nightly"

#cd $localpath

time $bin/1-make-pbuilder-user.sh \
-d "$debver" -s "$suite" -k "$kohaver" \
-p "$aptrepo" \
-r "$repo" \
-z "$repopath" \
-x "$suffix" \
-e "$bin" \
-c \
-b \
-n \
-f \
-l "$localpath" \
-t "$tmpdir" \
-a 'local' \
 &> ~/$tmpdir/$suite.log
#-f \
#-a 'remote' \
#-f \

rc=$?
if [ $rc -eq 0 ]; then
    cp ~/$tmpdir/$suite.log  $repopath/deb/$suite/build.log
    figlet success
else
    figlet fails
fi
date
