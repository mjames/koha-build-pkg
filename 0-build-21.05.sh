#!/bin/bash -x

date

#LD_LIBRARY_PATH=/usr/lib/libeatmydata
#LD_PRELOAD=libeatmydata.so
#export LD_LIBRARY_PATH LD_PRELOAD

localpath="/home/mason/pb/pb-master/koha"

kohaver="21.05.22"
#kohaver="master"
kohaver_incr="1"

#debver="stretch"
debver="buster"
#suite="oldoldstable"
suite="21.05"
repo="koha"
urgency="high"
#urgency="medium"
#urgency="low"

repopath="/home/mason/nfs/apt/kc/$repo"
#aptrepo="deb [trusted=yes] http://deb.kohaaloha.com/kc/koha $suite main"
#aptrepo="deb [trusted=yes] http://debian.koha-community.org/koha-staging $suite main"
aptrepo="deb http://debian.koha-community.org/$repo $suite main"

suffix="kc"
tmpdir="tmp-$suffix"

if [ ! -d ~/$tmpdir ] ; then
    mkdir ~/$tmpdir
fi

bin="/home/mason/git/bin-nightly"

#cd $localpath

time $bin/1-make-pbuilder-user.sh \
-d "$debver" -s "$suite" -k "$kohaver" \
-p "$aptrepo" \
-r "$repo" \
-z "$repopath" \
-x "$suffix" \
-e "$bin" \
-u "$urgency" \
-i "$kohaver_incr" \
\
-c \
-b \
\
-l "$localpath" \
-t "$tmpdir" \
-a 'local' \
 &> ~/$tmpdir/$suite.log
#-a 'remote' \
#-a 'local' \
#-b \
#-c \
##-f \

rc=$?
if [ $rc -eq 0 ]; then
    cp ~/$tmpdir/$suite.log  $repopath/deb/$suite/build.log
    figlet success
else
    figlet fail
fi
date
